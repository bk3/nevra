#!/usr/bin/python -tt

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# Copyright 2003 Duke University

from rpmUtils.miscutils import *
import rpmUtils.miscutils

def splitFilename(filename):
    """
    pass in a n-e:v-r.a style rpm fullname
    Return a name, version, release, epoch, arch tupple.  e.g. ::
        foo-1.0-1.i386.rpm returns {foo, 1.0, '', 1, i386}
        bind-32:9.10.2-2.P1.fc22.x86_64 returns (bind, 9.10.2, 32, 2.P1.fc22,x86_64)
    """


    if filename[-4:] == '.rpm':
      filename = filename[:-4]

    archIndex   = filename.rfind('.')
    relIndex    = filename[:archIndex].rfind('-')
    epochIndex  = filename[:relIndex].rfind('-')
    verIndex    = filename[epochIndex:relIndex].find(':')

    arch        = filename[archIndex+1:]
    rel         = filename[relIndex+1:archIndex]
    name        = filename[:epochIndex]

    if verIndex >= 0:
      verIndex += epochIndex
      epoch  = filename[epochIndex+1:verIndex]
      ver    = filename[verIndex+1:relIndex]
    else:
      epoch  = ''
      ver    = filename[epochIndex+1:relIndex]

    return name, ver, rel, epoch, arch

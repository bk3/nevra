#!/usr/bin/python -tt

import os
import sys
import NevraMiscUtils

sys.excepthook = lambda *args: None

if __name__ == "__main__":
  args = sys.argv[1:]
  if args == []:
    try:
      for rpm in sys.stdin:
        print NevraMiscUtils.splitFilename(rpm)
    except:
      pass
    finally:
      os._exit(0)
  else:
      for rpm in args:
        print NevraMiscUtils.splitFilename(rpm)

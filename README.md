# Extends rpmUtils.miscutils.splitFilename()

# Parses zero or more Nevra rpm names and return nvrea tupple(s)
#     (name, ver, rel, epoch, arch)

# ./test1.py bind-32:9.10.2-2.P1.fc22.x86_64
# ('bind', '9.10.2', '2.P1.fc22', '32', 'x86_64')

# rpm -qa | ./test1.py
# ...
# ('zlib', '1.2.7', '17.el7', '', 'x86_64\n')
